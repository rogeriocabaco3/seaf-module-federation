const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
    path.join(__dirname, 'tsconfig.json'),
    [/* mapped paths to share */]);

//O nosso é ligeiramente diferente

module.exports = {
    output: {
        uniqueName: "seafModuleFederation",
        publicPath: "auto"
    },
    optimization: {
        runtimeChunk: false
    },
    resolve: {
        alias: {
            ...sharedMappings.getAliases(),
        }
    },
    experiments: {
        outputModule: true
    },
    plugins: [
        new ModuleFederationPlugin({
            library: {type: "module"},

            // For remotes (please adjust)
            name: "hackathon-seaf",
            filename: "seaf.js",
            exposes: {
                './SeafModule': './src/app/app.module.ts',
            },

            // For hosts (please adjust)
            remotes: {
                '@one/core': '/one.js'

            },

            shared: share({
                "@angular/core": {singleton: true, strictVersion: false, requiredVersion: '15.2.0'},
                "@angular/common": {singleton: true, strictVersion: false, requiredVersion: '15.2.0'},
                "@angular/common/http": {singleton: true, strictVersion: false, requiredVersion: '15.2.0'},
                "@angular/router": {singleton: true, strictVersion: false, requiredVersion: '15.2.0'},

                ...sharedMappings.getDescriptors()
            })

        }),
        sharedMappings.getPlugin()
    ],
};
