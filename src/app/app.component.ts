import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

export interface ICar {
  id: number;
  brand: string;
  model: IModel;
  price: number;
  imageUrl: string;
}

export interface IModel {
  modelName: string;
  fuelType: string;
  transmission: string;
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "seaf-module-federation";

  cars: ICar[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getCars();
  }

  showDetails(id: number) {
    this.http.get(`/api/car/${id}`).subscribe((car: any) => {
      console.log(car);
    });
  }

  getCars() {
    this.http.get("/api/carsList").subscribe((cars: any) => {
      this.cars = cars;
      console.log("this.cars", this.cars);
    });
  }
}
