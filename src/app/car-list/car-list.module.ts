// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { CarListComponent } from './car-list.component';

@NgModule({
    imports: [

    ],
    declarations: [
        CarListComponent,
    ],
    exports: [
        CarListComponent,
    ]
})
export class CarListModule {

}
