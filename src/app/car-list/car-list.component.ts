import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ICar } from "../app.component";

@Component({
  selector: "car-list",
  templateUrl: "car-list.component.html",
  styleUrls: ["car-list.component.scss"],
})
export class CarListComponent implements OnInit {
  title = "seaf-module-federation";

  cars: ICar[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getCars();
  }

  getCars() {
    this.http.get("/api/carsList").subscribe((cars: any) => {
      this.cars = cars;
      console.log("this.cars", this.cars);
    });
  }
}
