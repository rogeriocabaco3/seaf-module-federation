import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { ButtonModule } from "primeng/button";
import { InputTextModule } from "primeng/inputtext";
import { TableModule } from "primeng/table";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { CarListComponent } from "./car-list/car-list.component";
import { CarDetailsComponent } from "./car-details/car-details.component";

@NgModule({
  declarations: [AppComponent, CarListComponent, CarDetailsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    HttpClientModule,
    CommonModule,
  ],
  providers: [],
})
export class AppModule {}
