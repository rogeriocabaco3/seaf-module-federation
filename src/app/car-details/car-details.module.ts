// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { CarDetailsComponent } from './car-details.component';

@NgModule({
    imports: [

    ],
    declarations: [
        CarDetailsComponent,
    ],
    exports: [
        CarDetailsComponent,
    ]
})
export class CarDetailsModule {

}
