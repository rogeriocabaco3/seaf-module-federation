import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { ICar } from "../app.component";
import { ActivatedRoute } from "@angular/router";
import { map } from "rxjs";

@Component({
  selector: "car-details",
  templateUrl: "car-details.component.html",
  styleUrls: ["car-details.component.scss"],
})
export class CarDetailsComponent implements OnInit {
  title = "seaf-module-federation";

  car!: ICar;

  constructor(private http: HttpClient, private activeRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params) => {
      console.log("params", params);
      this.showDetails(params["id"]);
    });
  }

  showDetails(id: number) {
    this.http.get(`/api/car/${id}`).subscribe((car: any) => {
      this.car = car;
      console.log(car);
    });
  }
}
